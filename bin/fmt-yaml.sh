#!/bin/bash

file=$1

command -v yq >/dev/null 2>&1 || { echo "yq is not installed.  Aborting." >&2; exit 1; }

yq -P $file > /tmp/${file##*/}-aaaa
mv /tmp/${file##*/}-aaaa $file
